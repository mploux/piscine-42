/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/08 18:32:44 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 19:36:08 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_prime(int nb)
{
	int i;
	int count;

	if (nb <= 0)
		return (0);
	i = 2;
	count = 0;
	while (i <= nb / 2)
	{
		if (nb % i == 0)
		{
			count++;
			break ;
		}
		i++;
	}
	if (count == 0 && nb != 1)
		return (1);
	else
		return (0);
}

int			ft_find_next_prime(int nb)
{
	int prime;
	int test;

	prime = 0;
	test = nb;
	while (!prime)
	{
		if (ft_is_prime(test))
			prime = test;
		test++;
	}
	return (prime);
}
