/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/07 15:25:32 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 22:10:01 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_iterative_power(int nb, int power)
{
	int i;
	int tmp_nb;

	if (power == 0)
		return (1);
	if (power < 0)
		return (0);
	i = 0;
	tmp_nb = nb;
	while (i++ < power - 1)
		nb *= tmp_nb;
	return (nb);
}
