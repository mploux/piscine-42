/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/07 14:19:49 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 21:40:45 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_iterative_factorial(int nb)
{
	int i;
	int result;

	if (nb == 0)
		return (1);
	if (nb < 0 || nb >= 13)
		return (0);
	i = 1;
	result = nb;
	while (i < nb)
	{
		result *= i;
		i++;
	}
	return (result);
}
