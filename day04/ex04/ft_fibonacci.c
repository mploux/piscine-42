/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/08 16:40:20 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 19:38:22 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_fibonacci(int index)
{
	if (index >= 47)
		return (0);
	if (index < 0)
		return (-1);
	if (index <= 1)
		return (index);
	else
		return (ft_fibonacci(index - 1) + ft_fibonacci(index - 2));
}
