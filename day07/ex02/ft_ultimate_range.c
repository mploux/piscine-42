/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/15 15:55:36 by mploux            #+#    #+#             */
/*   Updated: 2016/08/18 16:30:06 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;

	i = min;
	if (min >= max)
		return (0);
	if (!(*range = (int *)malloc(sizeof(int) * (max - min))))
		return (0);
	while (i < max)
	{
		(*range)[i - min] = i;
		i++;
	}
	return (max - min);
}
