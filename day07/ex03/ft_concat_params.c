/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/15 16:24:46 by mploux            #+#    #+#             */
/*   Updated: 2016/08/18 15:59:51 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_get_size(int argc, char **argv)
{
	int		size;
	int		i;
	int		j;

	i = 0;
	j = 0;
	size = 0;
	while (i < argc)
	{
		while (argv[i][j])
		{
			size++;
			j++;
		}
		j = 0;
		i++;
	}
	return (size);
}

void	ft_put_word(int *k, int *i, char **result, char ***argv)
{
	int j;

	j = 0;
	while ((*argv)[*k][j])
	{
		(*result)[*i] = (*argv)[*k][j];
		j++;
		(*i)++;
	}
}

char	*ft_concat_params(int argc, char **argv)
{
	char	*result;
	int		size;
	int		i;
	int		k;

	i = 0;
	k = 1;
	size = ft_get_size(argc, argv);
	result = (char *)malloc(sizeof(char) * (size + argc));
	while (k < argc)
	{
		ft_put_word(&k, &i, &result, &argv);
		if (k != argc - 1)
			result[i] = '\n';
		else
			result[i] = '\0';
		i++;
		k++;
	}
	return (result);
}
