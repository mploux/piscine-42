/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/17 13:08:01 by mploux            #+#    #+#             */
/*   Updated: 2016/08/19 05:35:15 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_par.h"

char					*ft_strdup(char *src)
{
	char	*copy;
	int		size;
	int		i;

	size = 0;
	while (src[size])
		size++;
	copy = (char *)malloc(sizeof(char) * (size + 1));
	i = 0;
	while (src[i])
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}

struct s_stock_par		ft_fill_params(int s, char *str, char *cpy, char **tab)
{
	t_stock_par result;

	result.size_param = s;
	result.str = str;
	result.copy = cpy;
	result.tab = tab;
	return (result);
}

struct s_stock_par		*ft_param_to_tab(int ac, char **av)
{
	int					i;
	int					size;
	char				*copy;
	char				**tab;
	t_stock_par			*result;

	if (!(result = (t_stock_par *)malloc(sizeof(*result) * (ac + 1))))
		return (0);
	i = 0;
	while (i < ac)
	{
		size = 0;
		while (av[i][size])
			size++;
		copy = ft_strdup(av[i]);
		tab = ft_split_whitespaces(av[i]);
		result[i] = ft_fill_params(size, av[i], copy, tab);
		i++;
	}
	result[i] = ft_fill_params(0, 0, 0, 0);
	return (result);
}
