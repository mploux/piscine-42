/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/15 18:57:17 by mploux            #+#    #+#             */
/*   Updated: 2016/08/20 15:48:46 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			ft_skip(char c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	return (0);
}

int			ft_count(char *str)
{
	int		count;
	int		s;

	count = 0;
	s = 0;
	while (str[++s])
	{
		while (ft_skip(str[s]) && ft_skip(str[s + 1]))
			s++;
		if (ft_skip(str[s]) && !ft_skip(str[s + 1]) && str[s + 1] != '\0')
			count++;
	}
	return (count);
}

void		ft_put_word(int i, int *k, char *str, char ***result)
{
	int j;
	int l;

	j = 1;
	while (++(*k) && !ft_skip(str[*k]) && str[*k] != '\0')
		j++;
	(*result)[i] = (char *)malloc(sizeof(char) * (j + 1));
	l = 0;
	while (l < j)
	{
		(*result)[i][l] = str[*k - j + l];
		l++;
	}
	(*result)[i][l] = '\0';
	if (str[*k] != '\0')
		(*k)++;
}

char		**ft_split_whitespaces(char *str)
{
	char	**result;
	int		count;
	int		i;
	int		k;

	i = 0;
	k = 0;
	count = ft_count(str);
	result = (char **)malloc(sizeof(char *) * (count + 2));
	while (i < count + 1)
	{
		while (ft_skip(str[k]))
			k++;
		if (str[k] == '\0')
			break ;
		ft_put_word(i, &k, str, &result);
		i++;
	}
	result[i] = NULL;
	return (result);
}
