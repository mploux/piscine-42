/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_manager.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adartigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 22:24:15 by adartigu          #+#    #+#             */
/*   Updated: 2016/08/14 22:24:18 by adartigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cell.h"

void			ft_clear_x(int x, char c, char tab[9][9][10])
{
	int i;

	i = -1;
	while (++i < 9)
		tab[x][i][c - '0'] = '.';
}

void			ft_clear_y(int y, char c, char tab[9][9][10])
{
	int i;

	i = -1;
	while (++i < 9)
		tab[i][y][c - '0'] = '.';
}

void			ft_clear_block(int x, int y, char c, char tab[9][9][10])
{
	int i;
	int j;
	int xoffs;
	int yoffs;

	xoffs = x / 3 * 3 - 1;
	yoffs = y / 3 * 3 - 1;
	i = xoffs;
	j = yoffs;
	while (i++ < xoffs + 3)
	{
		while (j++ < yoffs + 3)
			tab[i][j][c - '0'] = '.';
		j = yoffs;
	}
}

void			ft_clear_solus(int x, int y, char c, char tab[9][9][10])
{
	int i;

	if (c == '.')
		return ;
	i = 0;
	ft_clear_x(x, c, tab);
	ft_clear_y(y, c, tab);
	ft_clear_block(x, y, c, tab);
	if (tab[x][y][0] != '.')
		while (++i <= 9)
			tab[x][y][i] = '.';
}
