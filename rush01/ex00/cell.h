/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cell.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 22:32:34 by mploux            #+#    #+#             */
/*   Updated: 2016/08/14 22:32:35 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CELL_H
# define CELL_H

typedef struct	s_cell
{
	int		x;
	int		y;
	int		k;
	char	solution;
	int		count;
}				t_cell;

#endif
