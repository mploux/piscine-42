/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_solver.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adartigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 22:25:30 by adartigu          #+#    #+#             */
/*   Updated: 2016/08/14 22:25:33 by adartigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cell.h"

void			ft_clear_solus(int x, int y, char c, char tab[9][9][10]);

int				ft_solve_blocks(char tab[9][9][10]);

int				ft_solve_xy(int i, int x, t_cell *cell, char tab[9][9][10]);

int				ft_solve_x(char tab[9][9][10]);

int				ft_solve_y(char tab[9][9][10]);

void			ft_check_cell(int x, int y, char tab[9][9][10], t_cell *cell)
{
	if (tab[x][y][cell->k] != '.')
	{
		cell->x = x;
		cell->y = y;
		cell->solution = tab[x][y][cell->k];
		cell->count++;
	}
}

t_cell			ft_solve_block_char(int x, int y, char tab[9][9][10])
{
	int		i;
	int		j;
	int		k;
	t_cell	cell;

	i = -1;
	j = -1;
	k = 0;
	cell.count = 0;
	while (++k < 10)
	{
		cell.k = k;
		while (++i < 3)
		{
			while (++j < 3)
				ft_check_cell(x * 3 + i, y * 3 + i, tab, &cell);
			j = -1;
		}
		i = -1;
		if (cell.count == 1)
			return (cell);
		cell.count = 0;
	}
	return (cell);
}

void			ft_calc_solus(char tab[9][9][10])
{
	int x;
	int y;

	x = -1;
	y = -1;
	while (++y < 9)
	{
		while (++x < 9)
		{
			ft_clear_solus(x, y, tab[x][y][0], tab);
		}
		x = -1;
	}
}

void			ft_solve(char tab[9][9][10])
{
	int valid;

	valid = 1;
	while (valid != 0)
	{
		valid = 0;
		valid += ft_solve_blocks(tab);
		valid += ft_solve_x(tab);
		valid += ft_solve_y(tab);
	}
}
