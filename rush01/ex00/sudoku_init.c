/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mathierr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 22:49:58 by mathierr          #+#    #+#             */
/*   Updated: 2016/08/14 22:50:01 by mathierr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void			ft_fill_args(char **argv, char tab[9][9][10])
{
	int x;
	int y;

	x = -1;
	y = -1;
	while (++y < 9)
	{
		while (++x < 9)
			tab[x][y][0] = argv[y + 1][x];
		x = -1;
	}
}

void			ft_fill_solus(char tab[9][9][10])
{
	int x;
	int y;
	int i;

	x = -1;
	y = -1;
	i = 0;
	while (++y < 9)
	{
		while (++x < 9)
		{
			while (++i < 10)
				tab[x][y][i] = '0' + i;
			i = 0;
		}
		x = -1;
	}
}
