/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 11:35:30 by mploux            #+#    #+#             */
/*   Updated: 2016/08/14 11:35:33 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "cell.h"

void			ft_fill_args(char **argv, char tab[9][9][10]);

void			ft_fill_solus(char tab[9][9][10]);

void			ft_calc_solus(char tab[9][9][10]);

void			ft_solve(char tab[9][9][10]);

void			ft_putchar(char c);

void			ft_draw_grid(char tab[9][9][10], int layer);

int				ft_error(void)
{
	write(1, "Erreur\n", 7);
	return (0);
}

int				ft_is_solved(char tab[9][9][10])
{
	int x;
	int y;

	x = -1;
	y = -1;
	while (++y < 9)
	{
		while (++x < 9)
		{
			if (tab[x][y][0] == '.')
				return (0);
			if (tab[x][y][0] > '9')
				return (0);
			if (tab[x][y][0] < '0')
				return (0);
		}
		x = -1;
	}
	return (1);
}

int				ft_is_error(int argc, char **argv)
{
	int i;
	int j;
	int sum;

	i = 1;
	j = 0;
	sum = 0;
	while (i < argc)
	{
		while (argv[i][j])
		{
			if (argv[i][j] != '.')
				sum++;
			j++;
		}
		if (j != 9)
			return (1);
		j = 0;
		i++;
	}
	if (argc < 10)
		return (1);
	if (sum < 17)
		return (1);
	return (0);
}

int				main(int argc, char **argv)
{
	char	tab[9][9][10];

	if (ft_is_error(argc, argv))
		return (ft_error());
	ft_fill_args(argv, tab);
	ft_fill_solus(tab);
	ft_calc_solus(tab);
	ft_solve(tab);
	if (!ft_is_solved(tab))
		return (ft_error());
	ft_draw_grid(tab, 0);
	return (0);
}
