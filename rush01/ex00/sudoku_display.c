/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_display.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mathierr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 23:00:39 by mathierr          #+#    #+#             */
/*   Updated: 2016/08/14 23:00:44 by mathierr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

void			ft_draw_grid(char tab[9][9][10], int layer)
{
	int x;
	int y;

	x = -1;
	y = -1;
	while (++y < 9)
	{
		while (++x < 9)
		{
			ft_putchar(tab[x][y][layer]);
			if (x != 8)
				ft_putchar(' ');
		}
		ft_putchar('\n');
		x = -1;
	}
}
