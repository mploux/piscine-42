/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku_solver_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adartigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 22:20:59 by adartigu          #+#    #+#             */
/*   Updated: 2016/08/14 22:21:03 by adartigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cell.h"

void			ft_check_cell(int x, int y, char tab[9][9][10], t_cell *cell);

void			ft_clear_solus(int x, int y, char c, char tab[9][9][10]);

t_cell		ft_solve_block_char(int x, int y, char tab[9][9][10]);

int				ft_solve_blocks(char tab[9][9][10])
{
	int		i;
	int		j;
	t_cell	cell;
	int		used;

	i = -1;
	j = -1;
	used = 0;
	while (++i < 3)
	{
		while (++j < 3)
		{
			cell = ft_solve_block_char(i, j, tab);
			if (cell.count == 1)
			{
				tab[cell.x][cell.y][0] = cell.solution;
				ft_clear_solus(cell.x, cell.y, cell.solution, tab);
				used++;
			}
		}
		j = -1;
	}
	return (used);
}

int				ft_solve_xy(int i, int x, t_cell *cell, char tab[9][9][10])
{
	int k;
	int j;

	k = 0;
	j = -1;
	while (++k < 10)
	{
		cell->k = k;
		while (++j < 9)
			if (x)
				ft_check_cell(j, i, tab, cell);
			else
				ft_check_cell(i, j, tab, cell);
		if (cell->count == 1)
		{
			tab[cell->x][cell->y][0] = cell->solution;
			ft_clear_solus(cell->x, cell->y, cell->solution, tab);
			return (1);
		}
		cell->count = 0;
		j = -1;
	}
	k = 0;
	return (0);
}

int				ft_solve_x(char tab[9][9][10])
{
	int		x;
	int		y;
	int		k;
	t_cell	cell;

	x = -1;
	y = -1;
	k = 0;
	cell.count = 0;
	while (++y < 9)
		if (ft_solve_xy(y, 1, &cell, tab))
			return (1);
	return (0);
}

int				ft_solve_y(char tab[9][9][10])
{
	int		x;
	int		y;
	int		k;
	t_cell	cell;

	x = -1;
	y = -1;
	k = 0;
	cell.count = 0;
	while (++x < 9)
		if (ft_solve_xy(x, 0, &cell, tab))
			return (1);
	return (0);
}
