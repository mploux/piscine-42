/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 22:13:04 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 22:23:38 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_list_merge(t_list **begin_list1, t_list *begin_list2)
{
	t_list *tmp;

	if (begin_list1 == NULL)
		return ;
	tmp = *begin_list1;
	while (tmp != NULL)
		tmp = tmp->next;
	tmp = begin_list2;
	return ;
}
