/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_foreach_if.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 05:12:46 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 04:17:55 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_foreach_if(t_list *begin_list,
			void (*f)(void *), void *data_ref, int (*cmp)())
{
	t_list *tmp;

	if (begin_list == NULL)
		return ;
	tmp = begin_list;
	while (tmp != NULL)
	{
		if (!(*cmp)(tmp->data, data_ref))
			(*f)(tmp->data);
		tmp = tmp->next;
	}
}
