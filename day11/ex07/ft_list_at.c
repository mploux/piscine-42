/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 00:19:28 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 16:58:00 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

t_list		*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	unsigned int	i;
	t_list			*tmp;

	if (begin_list == NULL)
		return (0);
	i = 0;
	tmp = begin_list;
	while (tmp != NULL)
	{
		if (i == nbr)
			return (tmp);
		tmp = tmp->next;
		i++;
	}
	return (0);
}
