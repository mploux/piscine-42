/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_find.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 04:18:18 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 04:45:23 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

t_list		*ft_list_find(t_list *begin_list, void *data_ref, int (*cmp)())
{
	t_list *tmp;

	if (begin_list == NULL)
		return (0);
	tmp = begin_list;
	while (tmp != NULL)
	{
		if (!(*cmp)(tmp->data, data_ref))
			return (tmp);
		tmp = tmp->next;
	}
	return (0);
}
