/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 04:09:55 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 20:45:31 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_push_front(t_list **begin_list, void *data)
{
	t_list *tmp;

	if (begin_list == NULL)
		return ;
	tmp = ft_create_elem(data);
	if (*begin_list)
		tmp->next = *begin_list;
	else
		tmp->next = NULL;
	*begin_list = tmp;
}

t_list		*ft_list_push_params(int ac, char **av)
{
	t_list	*list;
	int		i;

	list = NULL;
	i = 0;
	while (i < ac)
	{
		ft_push_front(&list, av[i]);
		i++;
	}
	return (list);
}
