/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 04:18:18 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 22:10:01 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list *tmp;
	t_list *prev;
	t_list *next;

	if (begin_list == NULL)
		return ;
	tmp = *begin_list;
	while (tmp->next != NULL)
	{
		prev = tmp;
		tmp = tmp->next;
		next = tmp->next;
		if (!(*cmp)(tmp->data, data_ref))
		{
			if (next)
				prev->next = next;
			free(tmp);
			return ;
		}
	}
	if (next)
		begin_list = &prev;
	return ;
}
