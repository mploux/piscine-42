/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 03:43:22 by mploux            #+#    #+#             */
/*   Updated: 2016/08/22 20:44:10 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *tmp;

	if (begin_list == NULL)
		return ;
	tmp = ft_create_elem(data);
	if (*begin_list)
		tmp->next = *begin_list;
	else
		tmp->next = NULL;
	*begin_list = tmp;
}
