/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 04:58:47 by mploux            #+#    #+#             */
/*   Updated: 2016/08/23 02:46:29 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void		ft_list_clear(t_list **begin_list)
{
	t_list	*next;

	if (begin_list == NULL || *begin_list == NULL)
		return ;
	while (*begin_list)
	{
		next = (*begin_list)->next;
		free(*begin_list);
		*begin_list = next;
	}
}
