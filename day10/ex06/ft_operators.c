/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operators.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 15:28:34 by mploux            #+#    #+#             */
/*   Updated: 2016/08/23 04:14:43 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_operators.h"
#include "ft_error.h"

int			ft_add(int a, int b)
{
	return (a + b);
}

int			ft_sub(int a, int b)
{
	return (a - b);
}

int			ft_mul(int a, int b)
{
	return (a * b);
}

int			ft_div(int a, int b)
{
	if (b == 0)
	{
		ft_error("Stop: division by zero\n");
		return (-1);
	}
	return (a / b);
}

int			ft_mod(int a, int b)
{
	if (b == 0)
	{
		ft_error("Stop: modulo by zero\n");
		return (-1);
	}
	return (a % b);
}
