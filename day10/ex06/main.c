/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 05:08:26 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 19:48:41 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_operators.h"
#include "ft_error.h"

int		ft_atoi(char *str)
{
	int i;
	int signe;
	int value;

	i = 0;
	signe = 1;
	value = 0;
	while (str[i] <= ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		signe = i;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		value *= 10;
		value += str[i] - '0';
		i++;
	}
	return (value * (str[signe] == '-' ? -1 : 1));
}

void	ft_putnbr(int nb)
{
	long n;

	n = nb;
	if (n < 0)
	{
		n *= -1;
		ft_putchar('-');
	}
	if (n < 10)
	{
		ft_putchar(n + '0');
	}
	if (n >= 10)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
}

int		ft_do_op(int a, int b, char *op)
{
	if (*op == '+')
		return (ft_add(a, b));
	else if (*op == '-')
		return (ft_sub(a, b));
	else if (*op == '*')
		return (ft_mul(a, b));
	else if (*op == '/')
		return (ft_div(a, b));
	else if (*op == '%')
		return (ft_mod(a, b));
	else
		return (0);
}

int		main(int argc, char **argv)
{
	int op;

	if (argc != 4)
		return (0);
	op = ft_do_op(ft_atoi(argv[1]), ft_atoi(argv[3]), argv[2]);
	if (op == -1)
		return (0);
	else
	{
		ft_putnbr(op);
		ft_putchar('\n');
	}
	return (0);
}
