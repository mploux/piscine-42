/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_foreach.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/19 14:02:15 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 19:50:27 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_foreach(int *tab, int length, void (*f)(int))
{
	int i;

	i = 0;
	if (!tab)
		return ;
	while (i < length)
	{
		(*f)(tab[i]);
		i++;
	}
}
