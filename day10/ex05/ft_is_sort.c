/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 05:02:27 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 19:51:23 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int i;
	int s;

	i = 0;
	s = 0;
	while (i < length - 1)
	{
		if ((*f)(tab[i], tab[i + 1]) >= 0 && s == 0)
			s = 1;
		else if ((*f)(tab[i], tab[i + 1]) < 0 && s == 0)
			s = -1;
		if (s == 1 && (*f)(tab[i], tab[i + 1]) < 0)
			return (0);
		if (s == -1 && (*f)(tab[i], tab[i + 1]) >= 0)
			return (0);
		i++;
	}
	return (1);
}
