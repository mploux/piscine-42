/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 13:51:05 by mploux            #+#    #+#             */
/*   Updated: 2016/08/24 12:56:34 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_insert_data(t_btree **root, void *item,
		int (*cmpf)(void *, void *))
{
	if (root)
	{
		if (!(*root))
			*root = btree_create_node(item);
		else if ((*cmpf)((*root)->item, item) > 0)
		{
			if ((*root)->left)
				btree_insert_data(&((*root)->left), item, cmpf);
			else
				(*root)->left = btree_create_node(item);
		}
		else if ((*cmpf)((*root)->item, item) <= 0)
		{
			if ((*root)->right)
				btree_insert_data(&((*root)->right), item, cmpf);
			else
				(*root)->right = btree_create_node(item);
		}
	}
}
