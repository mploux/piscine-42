/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_create_node.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 02:53:04 by mploux            #+#    #+#             */
/*   Updated: 2016/08/23 03:35:32 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_btree.h"

t_btree			*btree_create_node(void *item)
{
	t_btree	*tree;

	if (!(tree = (t_btree*)malloc(sizeof(t_btree))))
		return (NULL);
	tree->item = item;
	tree->left = NULL;
	tree->right = NULL;
	return (tree);
}
