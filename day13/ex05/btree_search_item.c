/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_search_item.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 16:18:13 by mploux            #+#    #+#             */
/*   Updated: 2016/08/24 12:56:12 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	*btree_search_item(t_btree *root,
		void *data_ref, int (*cmpf)(void *, void *))
{
	void *left;
	void *right;

	if (root)
	{
		left = btree_search_item(root->left, data_ref, cmpf);
		if (left)
			return (left);
		if (!(*cmpf)(root->item, data_ref))
			return (root->item);
		right = btree_search_item(root->right, data_ref, cmpf);
		if (right)
			return (right);
	}
	return (0);
}
