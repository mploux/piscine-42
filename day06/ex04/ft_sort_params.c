/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 16:06:47 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 16:06:49 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

int		ft_cmp(char *s1, char *s2)
{
	while ((*s1 == *s2) && (*s1 && *s2))
	{
		s1++;
		s2++;
	}
	if (*s1 - *s2 < 0)
		return (0);
	return (1);
}

void	ft_sort(int length, char **args)
{
	int		i;
	int		swaped;
	char	*tmp;

	i = 2;
	swaped = 1;
	while (swaped)
	{
		swaped = 0;
		while (i < length)
		{
			if (ft_cmp(args[i - 1], args[i]))
			{
				tmp = args[i - 1];
				args[i - 1] = args[i];
				args[i] = tmp;
				swaped = 1;
			}
			i++;
		}
		i = 2;
	}
}

int		main(int argc, char **argv)
{
	int i;

	i = 0;
	ft_sort(argc, argv);
	while (i++ < argc - 1)
	{
		while (*argv[i]++)
			ft_putchar(*(argv[i] - 1));
		ft_putchar('\n');
	}
	return (0);
}
