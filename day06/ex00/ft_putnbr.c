/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 12:52:53 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 12:52:56 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_putchar(char c);

void		ft_putnbr(int nb)
{
	if (nb > 0)
	{
		if (nb / 10 == 0)
		{
			ft_putchar('0' + (nb % 10));
			return ;
		}
		ft_putnbr(nb / 10);
		ft_putchar('0' + (nb % 10));
	}
	else if (nb < 0)
	{
		nb *= -1;
		ft_putchar('-');
		ft_putnbr(nb);
	}
	else
	{
		ft_putchar('0');
		return ;
	}
}
