/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/06 09:22:15 by mploux            #+#    #+#             */
/*   Updated: 2016/08/07 22:07:59 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_print(int x, int width, int size, int offs)
{
	int door_width;
	int is_door_width;
	int is_handle;
	int hand_h;

	door_width = 0;
	door_width += (size + 1) / 2;
	is_door_width = x > width / 2 - door_width && x < width / 2 + door_width;
	is_handle = x == width / 2 + door_width - 2;
	hand_h = offs * 2 == (door_width + size / 2 - ((size - 1) % 2)) - 1;
	if (is_door_width)
		if (offs < size - offs % 2)
			if (door_width >= 3)
				if (is_handle && hand_h)
					ft_putchar('$');
				else
					ft_putchar('|');
			else
				ft_putchar('|');
		else
			ft_putchar('*');
	else
		ft_putchar('*');
}

int			ft_scanline(int offs, int *width, int size)
{
	int x_offs;
	int x;

	x_offs = 0;
	x = 0;
	*width += 2;
	if (offs <= -1)
		return (*width);
	while (x_offs++ < offs)
		ft_putchar(' ');
	ft_putchar('/');
	while (x++ < *width - 1)
		ft_print(x, *width, size, offs);
	ft_putchar('\\');
	ft_putchar('\n');
	return (*width);
}

int			sastantua_algo(int s, int base_w)
{
	int i;
	int y;
	int width;
	int height;
	int final_width;

	i = -1;
	y = 0;
	width = 0;
	height = 3;
	final_width = 0;
	while (i++ < s - 1)
	{
		while (y++ < height)
			final_width = ft_scanline(base_w / 2 - width / 2 - 1, &width, s);
		width += 4 + (i / 2) * 2;
		height++;
		y = 0;
	}
	return (final_width);
}

void		sastantua(int size)
{
	int base_width;

	if (size <= 0)
		return ;
	base_width = sastantua_algo(size, 0);
	sastantua_algo(size, base_width);
}
