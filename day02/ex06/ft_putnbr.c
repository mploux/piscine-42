/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/04 14:59:40 by mploux            #+#    #+#             */
/*   Updated: 2016/08/05 11:49:42 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_putchar(char c);

int			ft_int_length(int nb)
{
	int length;

	length = 1;
	while (nb > 9)
	{
		nb /= 10;
		length++;
	}
	return (length);
}

void		ft_putnbr(int nb)
{
	int length;
	int i;
	int ic;

	length = ft_int_length(nb);
	i = 0;
	ic = 0;
	while (i < length)
	{
		ic = nb % 10;
		nb /= 10;
		ft_putchar('0' + ic);
		i++;
	}
}
