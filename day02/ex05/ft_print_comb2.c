/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/03 18:14:16 by mploux            #+#    #+#             */
/*   Updated: 2016/08/05 16:21:30 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_putchar(char c);

void		ft_print_number(int n)
{
	int a;
	int b;

	a = n / 10;
	b = n % 10;
	ft_putchar('0' + a);
	ft_putchar('0' + b);
}

void		ft_print_all(int a, int b)
{
	ft_print_number(a);
	ft_putchar(' ');
	ft_print_number(b);
}

void		ft_print_comb2(void)
{
	int i;
	int a;
	int b;

	i = 1;
	a = 0;
	b = 0;
	while (i < 10000 - 100)
	{
		a = i / 100;
		b = i % 100;
		ft_print_all(a, b);
		if (i != 10000 - 100 - 1)
		{
			ft_putchar(',');
			ft_putchar(' ');
		}
		i++;
	}
}
