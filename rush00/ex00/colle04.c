/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle03.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: slegras <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/05 23:11:17 by slegras           #+#    #+#             */
/*   Updated: 2016/08/05 23:11:36 by slegras          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_scanline(char start, char filler, char end, long n)
{
	long i;

	i = 0;
	ft_putchar(start);
	while (++i < n - 1)
	{
		ft_putchar(filler);
	}
	if (n > 1)
		ft_putchar(end);
	ft_putchar('\n');
}

int		ft_verif(long x, long y)
{
	if (x <= 0 || y <= 0)
	{
		return (0);
	}
	return (1);
}

void	colle(long x, long y)
{
	long i;

	if (!ft_verif(x, y))
		return ;
	i = 0;
	ft_scanline('A', 'B', 'C', x);
	while (++i < y - 1)
	{
		ft_scanline('B', ' ', 'B', x);
	}
	if (y > 1)
		ft_scanline('C', 'B', 'A', x);
}
