/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_first_ligne.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <cpoulet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 16:23:57 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 14:42:12 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"
#include "ft_error.h"

char	*ft_redim(char *str, int length, char c)
{
	char	*tmp;
	int		i;

	i = 0;
	tmp = malloc(sizeof(char) * length + 1);
	if (!tmp)
		ft_error("map error");
	if (str)
	{
		while (str[i] != '\0')
		{
			tmp[i] = str[i];
			i++;
		}
		free(str);
	}
	tmp[i] = c;
	tmp[i + 1] = '\0';
	return (tmp);
}

int		ft_first_ligne(t_map *map)
{
	char	c;
	int		ret;
	int		i;

	map->first_ligne = 0;
	i = 1;
	ret = read(map->fd, &c, 1);
	while (c != '\n' && ret == 1)
	{
		if (c != map->empty && c != map->block)
			return (ft_error("map error"));
		map->first_ligne = ft_redim(map->first_ligne, i, c);
		ret = read(map->fd, &c, 1);
		i++;
	}
	map->width = i - 1;
	if (ret == 0 || map->first_ligne == 0)
		return (ft_error("map error"));
	return (0);
}
