/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 16:17:54 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 11:39:41 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int		ft_error(char *error_msg)
{
	while (*(error_msg++))
		write(1, error_msg - 1, 1);
	write(1, "\n", 1);
	return (1);
}

int		ft_err(char *error_msg, char c)
{
	while (*(error_msg++))
		write(1, error_msg - 1, 1);
	write(1, ": ", 2);
	write(1, &c, 1);
	exit(0);
}
