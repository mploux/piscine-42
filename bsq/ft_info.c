/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_info.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 20:37:54 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 14:45:22 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"
#include "ft_error.h"

void	ft_height(t_map *map)
{
	int i;
	int nbr;

	i = 0;
	nbr = 0;
	while (i < map->nbr_info - 4)
	{
		nbr = nbr * 10 + map->info[i] - '0';
		i++;
	}
	map->height = nbr;
}

int		ft_info_ligne(t_map *map)
{
	char	c;
	int		ret;
	int		i;

	map->info = 0;
	i = 1;
	ret = read(map->fd, &c, 1);
	while (c != '\n' && ret == 1)
	{
		map->info = ft_redim(map->info, i, c);
		ret = read(map->fd, &c, 1);
		i++;
	}
	if (ret == 0 || i < 5)
		return (ft_error("map error"));
	map->nbr_info = i;
	ft_height(map);
	map->empty = map->info[i - 4];
	map->block = map->info[i - 3];
	map->full = map->info[i - 2];
	return (0);
}
