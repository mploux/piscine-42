/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loader.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 22:09:38 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 14:38:57 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_loader.h"

int		ft_load_char(t_map *map, int x, int y, char c)
{
	if (c == map->empty || c == map->block)
	{
		map->map[y][x] = c;
		map->root[y][x] = 0;
	}
	else if (c == '\n' || c == '\0')
	{
		if (x != map->width)
			return (ft_error("map error"));
		map->map[y][x] = '\0';
	}
	else
		return (ft_error("map error"));
	return (0);
}

int		ft_load_line(t_map *map, int y)
{
	int		x;
	char	c;

	x = 0;
	if (!(map->map[y] = (char*)malloc(sizeof(char) * (map->width + 1))))
		ft_error("map error");
	if (!(map->root[y] = (int*)malloc(sizeof(int) * (map->width + 1))))
		ft_error("map error");
	while (x <= map->width)
	{
		if (y == 0)
		{
			if (ft_load_char(map, x, y, map->first_ligne[x]))
				return (1);
		}
		else if (read(map->fd, &c, 1))
		{
			if (ft_load_char(map, x, y, c))
				return (1);
		}
		else if (read(map->fd, &c, 1) == 0 && y < map->height)
			return (ft_error("map error"));
		x++;
	}
	return (0);
}

int		ft_load_grid(t_map *map)
{
	int		y;
	char	c;

	y = 0;
	if (!(map->map = (char**)malloc(sizeof(char*) * (map->height + 1))))
		ft_error("map error");
	if (!(map->root = (int**)malloc(sizeof(int*) * (map->height + 1))))
		ft_error("map error");
	while (y < map->height)
	{
		if (ft_load_line(map, y))
			return (1);
		y++;
	}
	if (read(map->fd, &c, 1))
		return (ft_error("map error"));
	map->map[y] = NULL;
	map->root[y] = NULL;
	return (0);
}

void	ft_clear_grid(t_map *map)
{
	int y;

	y = 0;
	while (y < map->height)
	{
		free(map->map[y]);
		free(map->root[y]);
		y++;
	}
	free(map->map);
	free(map->root);
	free(map->first_ligne);
	free(map->info);
}
