/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_weight.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <cpoulet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/25 03:13:21 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 15:05:05 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"

void	ft_size_one(t_map *map, t_cell *rslt, int y, int x)
{
	map->root[y][x] = 1;
	if (map->root[y][x] >= rslt->value)
	{
		rslt->value = 1;
		rslt->x = x;
		rslt->y = y;
	}
}

void	ft_comp(t_map *map, t_cell *rslt, int y, int x)
{
	map->root[y][x] = 1 + ft_min_value(map->root, x, y);
	if (map->root[y][x] >= rslt->value)
	{
		rslt->value = map->root[y][x];
		rslt->x = x;
		rslt->y = y;
	}
}

t_cell	ft_weight(t_map *map)
{
	int		x;
	int		y;
	t_cell	rslt;

	rslt.value = 0;
	y = map->height - 1;
	while (y-- >= 0)
	{
		x = map->width - 1;
		while (x-- >= 0)
		{
			if (map->map[y + 1][x + 1] == map->block)
				map->root[y + 1][x + 1] = 0;
			else if (y + 1 == map->height - 1 || x + 1 == map->width - 1)
				ft_size_one(map, &rslt, y + 1, x + 1);
			else
				ft_comp(map, &rslt, y + 1, x + 1);
		}
	}
	return (rslt);
}
