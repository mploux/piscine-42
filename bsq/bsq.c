/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <cpoulet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 15:07:26 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 14:46:31 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"
#include "ft_error.h"
#include "ft_display.h"
#include "ft_loader.h"

int		solve(t_map *map, t_cell *rslt)
{
	if (ft_info_ligne(map))
		return (1);
	if (ft_first_ligne(map))
		return (1);
	if (ft_load_grid(map))
		return (1);
	*rslt = ft_weight(map);
	ft_register_soluce(map, rslt->x, rslt->y, rslt->value);
	ft_display(map);
	ft_clear_grid(map);
	return (0);
}

int		main(int ac, char **av)
{
	int		i;
	t_map	map;
	t_cell	rslt;

	i = 1;
	if (ac < 2)
	{
		map.fd = 0;
		solve(&map, &rslt);
	}
	while (av[i])
	{
		map.fd = open(av[i], O_RDONLY);
		if (map.fd != -1)
			solve(&map, &rslt);
		else
			ft_error("map error");
		if (i < ac - 1)
			ft_putchar('\n');
		i++;
	}
	return (0);
}
