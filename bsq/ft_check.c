/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <cpoulet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 23:06:58 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 04:29:59 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_bsq.h"

int		ft_get_min(int a, int b)
{
	if (a < b)
		return (a);
	return (b);
}

int		ft_min_value(int **root, int x, int y)
{
	int bottom;
	int right;
	int diag;

	bottom = root[y + 1][x];
	right = root[y][x + 1];
	diag = root[y + 1][x + 1];
	return (ft_get_min(bottom, ft_get_min(right, diag)));
}
