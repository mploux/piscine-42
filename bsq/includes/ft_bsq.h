/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bsq.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpoulet <cpoulet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 15:17:25 by cpoulet           #+#    #+#             */
/*   Updated: 2016/08/25 15:00:41 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BSQ_H
# define FT_BSQ_H
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct	s_map
{
	int		fd;
	int		width;
	int		height;
	char	**map;
	char	empty;
	char	block;
	char	full;
	char	*first_ligne;
	char	*info;
	int		nbr_info;
	int		**root;
}				t_map;

typedef struct	s_cell
{
	int x;
	int y;
	int value;
}				t_cell;

void			ft_putchar(char c);
int				ft_first_ligne(t_map *map);
char			*ft_redim(char *str, int length, char c);
int				ft_info_ligne(t_map *map);
void			ft_register_soluce(t_map *map, int x, int y, int size);
t_cell			ft_weight(t_map *map);
int				ft_min_value(int **root, int x, int y);

#endif
