/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loader.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/24 22:10:16 by mploux            #+#    #+#             */
/*   Updated: 2016/08/25 11:53:16 by cpoulet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LOADER_H
# define FT_LOADER_H

# include "ft_error.h"
# include "ft_bsq.h"

int		ft_load_char(t_map *map, int x, int y, char c);
int		ft_load_line(t_map *map, int y);
int		ft_load_grid(t_map *map);
void	ft_clear_grid(t_map *map);

#endif
