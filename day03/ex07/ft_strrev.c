/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/05 12:33:29 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 11:24:07 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_strlen(char *str)
{
	int length;

	length = 0;
	while (str[length] != '\0')
		length++;
	return (length);
}

char		*ft_strrev(char *str)
{
	int		length;
	int		i;
	int		inv_i;
	char	tmp;

	length = ft_strlen(str);
	i = -1;
	while (i++ < length / 2 - 1)
	{
		inv_i = length - 1 - i;
		if (i == inv_i)
			break ;
		tmp = str[inv_i];
		str[inv_i] = str[i];
		str[i] = tmp;
	}
	return (str);
}
