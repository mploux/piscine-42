/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/06 22:04:15 by mploux            #+#    #+#             */
/*   Updated: 2016/08/09 11:55:21 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void		ft_sort_integer_table(int *tab, int size)
{
	int i;
	int tmp;
	int swaped;

	i = -1;
	tmp = 0;
	swaped = 1;
	while (swaped)
	{
		swaped = 0;
		while (i++ < size)
		{
			if (tab[i - 1] > tab[i])
			{
				tmp = tab[i - 1];
				tab[i - 1] = tab[i];
				tab[i] = tmp;
				swaped = 1;
			}
		}
		i = -1;
	}
}
