/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/06 20:31:26 by mploux            #+#    #+#             */
/*   Updated: 2016/08/08 22:57:48 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_strlen(char *str)
{
	int length;

	length = 0;
	while (str[length] != '\0')
		length++;
	return (length);
}

int			ft_is_nbr(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int			ft_check_char(char c, int index, int *signe)
{
	if (c == '-')
	{
		if (*signe == -1)
			return (2);
		*signe = -1;
		return (1);
	}
	else if (c == '+')
	{
		if (*signe == -1)
			return (2);
		*signe = 1;
		return (1);
	}
	else if (c >= 'a' && c <= 'z')
		return (2);
	else if (c == ' ')
		return (1);
	else
		return (1);
}

int			ft_atoi(char *str)
{
	int	i;
	int	length;
	int	value;
	int	signe;
	int error;

	length = ft_strlen(str);
	i = -1;
	value = 0;
	signe = 1;
	while (i++ < length)
	{
		error = ft_check_char(str[i], i, &signe);
		if (!ft_is_nbr(str[i]))
		{
			if (error == 1)
				continue ;
			else if (error == 2)
				break ;
		}
		value += str[i] - '0';
		if (i != length - 1 && ft_is_nbr(str[i + 1]))
			value *= 10;
	}
	return (value * signe);
}
