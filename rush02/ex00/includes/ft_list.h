/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 16:10:19 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 23:10:35 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

# include <stdlib.h>

typedef struct	s_list
{
	struct s_list	*next;
	char			data;
}				t_list;

void			ft_list_put(t_list **begin_list, char data);
t_list			*ft_list_at(t_list *begin_list, unsigned int nbr);
int				ft_list_size(t_list *list);
t_list			*ft_list_last(t_list *begin_list);
void			ft_list_clear(t_list **begin_list);
void			ft_list_foreach(t_list *begin_list, void (*f)(char));

#endif
