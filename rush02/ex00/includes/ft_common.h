/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_common.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 18:41:01 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 23:10:17 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_COMMON_H
# define FT_COMMON_H

# include <unistd.h>
# include "ft_list.h"

typedef struct	s_corners
{
	char	a;
	char	b;
	char	c;
}				t_corners;

typedef struct	s_data
{
	int		width;
	int		height;
	int		colle[5];
}				t_data;

t_list			*colle_00(long x, long y);
t_list			*colle_01(long x, long y);
t_list			*colle_02(long x, long y);
t_list			*colle_03(long x, long y);
t_list			*colle_04(long x, long y);
void			ft_result(t_data data);
int				ft_compare(t_list *list, t_list *colle, t_data *data, int i);

#endif
