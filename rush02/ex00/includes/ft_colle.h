/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colle.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 21:29:02 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 21:39:21 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_COLLE_H
# define FT_COLLE_H

t_corners		ft_get_corners(char a, char b, char c);
void			ft_scanline(t_corners corners, long n, t_list **list);
int				ft_verif(long x, long y);

#endif
