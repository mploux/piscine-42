/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gerhard <gerhard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 16:30:51 by gerhard           #+#    #+#             */
/*   Updated: 2016/08/21 23:13:15 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "ft_list.h"
#include "ft_common.h"
#include "ft_utils.h"

void			print(char c)
{
	write(1, &c, 1);
}

int				ft_error(void)
{
	ft_putstr("aucune\n");
	return (0);
}

t_data			ft_record(t_list **list)
{
	char	c;
	int		x;
	int		y;
	t_data	data;

	x = 0;
	y = 0;
	while (read(0, &c, 1))
	{
		if (c != '\n')
		{
			if (y == 0)
				x++;
		}
		else
			y++;
		ft_list_put(list, c);
	}
	data.width = x;
	data.height = y;
	return (data);
}

int				ft_test_colle(t_data *data, t_list *list, int id)
{
	int valid;

	valid = 0;
	if (id == 0)
		valid += ft_compare(list, colle_00(data->width, data->height), data, 0);
	else if (id == 1)
		valid += ft_compare(list, colle_01(data->width, data->height), data, 1);
	else if (id == 2)
		valid += ft_compare(list, colle_02(data->width, data->height), data, 2);
	else if (id == 3)
		valid += ft_compare(list, colle_03(data->width, data->height), data, 3);
	else if (id == 4)
		valid += ft_compare(list, colle_04(data->width, data->height), data, 4);
	return (valid);
}

int				main(void)
{
	t_list		*list;
	t_data		data;
	int			valid;

	data = ft_record(&list);
	if (data.width == 0 || data.height == 0)
		return (ft_error());
	valid = 0;
	valid += ft_test_colle(&data, list, 0);
	valid += ft_test_colle(&data, list, 1);
	valid += ft_test_colle(&data, list, 2);
	valid += ft_test_colle(&data, list, 3);
	valid += ft_test_colle(&data, list, 4);
	if (valid == 0)
		return (ft_error());
	ft_result(data);
	ft_list_clear(&list);
	return (0);
}
