/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_compare.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gerhard <gerhard@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 19:08:12 by gerhard           #+#    #+#             */
/*   Updated: 2016/08/21 23:07:15 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "ft_common.h"
#include "ft_list.h"
#include "ft_utils.h"

void		ft_result(t_data data)
{
	char	c;
	int		i;

	c = '0';
	i = 0;
	while (c <= '5')
	{
		if (i > 0 && data.colle[c - '0'] == 1)
			ft_putstr(" || ");
		if (data.colle[c - '0'] == 1)
		{
			ft_putstr("[colle-0");
			ft_putchar(c);
			ft_putstr("] [");
			ft_putnbr(data.width);
			ft_putstr("] [");
			ft_putnbr(data.height);
			ft_putchar(']');
			i++;
		}
		c++;
	}
	ft_putchar('\n');
}

int			ft_compare(t_list *list, t_list *colle, t_data *dat, int i)
{
	int		j;
	t_list	*first;

	first = colle;
	j = 0;
	dat->colle[i] = 0;
	while (list != NULL && colle != NULL)
	{
		if (list->data != colle->data && j != 0)
		{
			dat->colle[i] = 0;
			ft_list_clear(&colle);
			return (0);
		}
		list = list->next;
		colle = colle->next;
		j++;
	}
	dat->colle[i] = 1;
	if (first)
		ft_list_clear(&first);
	return (1);
}
