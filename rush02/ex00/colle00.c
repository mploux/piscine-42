/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle00.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 21:06:13 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 21:36:46 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include "ft_common.h"
#include "ft_colle.h"

t_list		*colle_00(long x, long y)
{
	t_list	*list;
	long	i;

	if (!ft_verif(x, y))
		return (NULL);
	list = NULL;
	i = 0;
	ft_scanline(ft_get_corners('o', '-', 'o'), x, &list);
	while (++i < y - 1)
	{
		ft_scanline(ft_get_corners('|', ' ', '|'), x, &list);
	}
	if (y > 1)
		ft_scanline(ft_get_corners('o', '-', 'o'), x, &list);
	return (list);
}
