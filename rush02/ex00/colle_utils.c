/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 21:28:32 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 21:36:33 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_common.h"

t_corners	ft_get_corners(char a, char b, char c)
{
	t_corners corners;

	corners.a = a;
	corners.b = b;
	corners.c = c;
	return (corners);
}

void		ft_scanline(t_corners corners, long n, t_list **list)
{
	long i;

	i = 0;
	ft_list_put(list, corners.a);
	while (++i < n - 1)
	{
		ft_list_put(list, corners.b);
	}
	if (n > 1)
		ft_list_put(list, corners.c);
	ft_list_put(list, '\n');
}

int			ft_verif(long x, long y)
{
	if (x <= 0 || y <= 0)
	{
		return (0);
	}
	return (1);
}
