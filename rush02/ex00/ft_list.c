/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 16:32:39 by mploux            #+#    #+#             */
/*   Updated: 2016/08/21 23:06:57 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

void		ft_list_put(t_list **begin_list, char data)
{
	t_list *tmp;

	if (begin_list == NULL || !(tmp = malloc(sizeof(t_list))))
		return ;
	tmp->data = data;
	if (*begin_list)
		tmp->next = *begin_list;
	else
		tmp->next = NULL;
	*begin_list = tmp;
}

t_list		*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	unsigned int	i;
	t_list			*tmp;

	if (begin_list == NULL)
		return (0);
	i = 0;
	tmp = begin_list;
	while (tmp != NULL)
	{
		if (i == nbr)
			return (tmp);
		tmp = tmp->next;
		i++;
	}
	return (0);
}

int			ft_list_size(t_list *begin_list)
{
	int		size;
	t_list	*tmp;

	if (begin_list == NULL)
		return (0);
	size = 0;
	tmp = begin_list;
	while (tmp != NULL)
	{
		tmp = tmp->next;
		size++;
	}
	return (size);
}

void		ft_list_clear(t_list **begin_list)
{
	t_list *a;
	t_list *b;

	if (begin_list == NULL)
		return ;
	a = *begin_list;
	while (a->next)
	{
		b = a->next;
		free(a);
		a = b;
	}
}

void		ft_list_foreach(t_list *begin_list, void (*f)(char))
{
	t_list *tmp;

	if (begin_list == NULL)
		return ;
	tmp = begin_list;
	while (tmp != NULL)
	{
		(*f)(tmp->data);
		tmp = tmp->next;
	}
}
