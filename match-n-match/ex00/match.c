/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   match.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/14 08:57:54 by mploux            #+#    #+#             */
/*   Updated: 2016/08/14 08:57:56 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int			ft_check_line(char *s1, char *s2, int star_count)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (s2[j])
	{
		while (s2[j] == '*' && s2[j + 1] == '*')
			j++;
		if (s1[i] != s2[j] && s2[j] != '*')
		{
			return (0);
		}
		printf("%c  %c  %d\n", s1[i], s2[j], (s1[i] == s2[j]));
		if (s1[i] == s2[j])
			j++;
		else if (s1[i + 1] == s2[j + 1])
			j++;
		i++;
	}
	return (1);
}

int			match(char *s1, char *s2)
{
	int match;
	int star_count;
	int i;

	i = 0;
	star_count = 0;
	while (s2[i])
		if (s2[i] == '*')
			star_count++;

	match = ft_check_line(s1, s2, star_count);

	if (match)
		return (1);
	return (0);
}
