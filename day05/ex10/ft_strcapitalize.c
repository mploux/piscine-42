/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <mploux@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 15:58:01 by mploux            #+#    #+#             */
/*   Updated: 2016/08/17 12:27:59 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_uppercase(char c)
{
	if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

int			ft_is_number(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int			ft_is_alpha(char c)
{
	if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		return (1);
	return (0);
}

int			ft_check(char c)
{
	if (ft_is_alpha(c) || ft_is_number(c))
		return (0);
	return (1);
}

char		*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (i == 0 || ft_check(str[i - 1]))
		{
			if (ft_is_alpha(str[i]) && !(str[i] >= 'A' && str[i] <= 'Z'))
				str[i] += ('A' - 'a');
		}
		else if (ft_is_uppercase(str[i]))
		{
			str[i] -= ('A' - 'a');
		}
		i++;
	}
	return (str);
}
