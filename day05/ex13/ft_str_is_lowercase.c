/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lower_case.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 09:13:26 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 09:13:45 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_lowercase(char c)
{
	if (c >= 'a' && c <= 'z')
		return (1);
	return (0);
}

int			ft_str_is_lowercase(char *str)
{
	int i;
	int alpha;

	i = 0;
	alpha = 1;
	while (str[i] != '\0')
	{
		if (ft_is_lowercase(str[i]))
			alpha = 1;
		else
			return (0);
		i++;
	}
	return (alpha);
}
