/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 21:01:08 by mploux            #+#    #+#             */
/*   Updated: 2016/08/09 21:01:15 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_lowercase(char c)
{
	if (c >= 'a' && c <= 'z')
		return (1);
	return (0);
}

int			ft_is_uppercase(char c)
{
	if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

int			ft_is_alpha(char c)
{
	if (ft_is_lowercase(c) || ft_is_uppercase(c))
		return (1);
	return (0);
}

int			ft_str_is_alpha(char *str)
{
	int i;
	int alpha;

	i = 0;
	alpha = 1;
	while (str[i] != '\0')
	{
		if (ft_is_alpha(str[i]))
			alpha = 1;
		else
			return (0);
		i++;
	}
	return (alpha);
}
