/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 07:56:52 by mploux            #+#    #+#             */
/*   Updated: 2016/08/15 14:56:09 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char		*ft_strstr(char *str, char *to_find)
{
	int i;
	int j;
	int k;

	i = 0;
	j = 0;
	while (str[i])
	{
		k = 1;
		j = 0;
		while (to_find[j])
		{
			if (!str[i + j] || to_find[j] != str[i + j])
				k = 0;
			j++;
		}
		if (k)
			return (&str[i]);
		i++;
	}
	return (0);
}
