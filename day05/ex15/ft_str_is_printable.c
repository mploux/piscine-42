/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 09:38:51 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 09:38:55 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_printabe(char c)
{
	if (c >= 32 && c <= 127)
		return (1);
	return (0);
}

int			ft_str_is_printable(char *str)
{
	int i;
	int alpha;

	i = 0;
	alpha = 1;
	while (str[i] != '\0')
	{
		if (ft_printabe(str[i]))
			alpha = 1;
		else
			return (0);
		i++;
	}
	return (alpha);
}
