/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 09:05:07 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 09:05:34 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_numeric(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	return (0);
}

int			ft_str_is_numeric(char *str)
{
	int i;
	int numeric;

	i = 0;
	numeric = 1;
	while (str[i] != '\0')
	{
		if (ft_is_numeric(str[i]))
			numeric = 1;
		else
			return (0);
		i++;
	}
	return (numeric);
}
