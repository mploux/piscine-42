/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 14:21:03 by mploux            #+#    #+#             */
/*   Updated: 2016/08/09 14:21:06 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_lowercase(char c)
{
	if (c >= 'a' && c <= 'z')
		return (1);
	return (0);
}

char		*ft_strupcase(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (ft_is_lowercase(str[i]))
			str[i] += ('A' - 'a');
		i++;
	}
	return (str);
}
