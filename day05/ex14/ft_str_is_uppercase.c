/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mploux <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 09:14:52 by mploux            #+#    #+#             */
/*   Updated: 2016/08/10 09:14:54 by mploux           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_is_uppercase(char c)
{
	if (c >= 'A' && c <= 'Z')
		return (1);
	return (0);
}

int			ft_str_is_uppercase(char *str)
{
	int i;
	int alpha;

	i = 0;
	alpha = 1;
	while (str[i] != '\0')
	{
		if (ft_is_uppercase(str[i]))
			alpha = 1;
		else
			return (0);
		i++;
	}
	return (alpha);
}
